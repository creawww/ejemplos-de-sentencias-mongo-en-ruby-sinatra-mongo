# MONGO

Correr los contenedores

    docker-compose up

entrar en el docker de la base de datos Mongo

    docker exec -it s_mongo /bin/bash

llamar al cliente de Mongo

    mongo

#### algunos comandos

    show databases;

    use test;

    db.holamundo.insert({idioma:"es_ES",mensaje:"Hola Mundo"});

    show collections;

    db.dropDatabase();

    db.usuario.find().pretty()

#### Comparadores

    $lt (less than),  < (menor estricto).
    $lte  (less than or  equal),  <=  (menor o igual).
    $gt (greather than), > (mayor estricto).
    $gte  (greather  than  or  equal), >=  (mayor o igual).



## PRACTICAR

## creacion y modificacion de datos

1-. Los usuarios de nuestra red social al registrarse nos dicen su nombre, apellidos, edad y sexo.
Además insertamos la fecha de alta en el momento del registro. Como identificador queremos
utilizar el tipo Long, no el ObjectID que nos da Mongo por defecto. Supongamos que se registran
dos usuarios Juan García Castellano, de 23 años de edad y Beatriz Perez Solaz de 27 años.
Escribe las sentencias para insertarlos en la colección "usuario", con identificadores 5 y 6
respectivamente.

    db.usuario.insert({_id:NumberLong(5), nombre:"Juan", apellidos:"García Castellano", edad: 23, sexo:"H", alta:new Date()})

    db.usuario.insert({_id:NumberLong(6), nombre:"Beatriz", apellidos:"Perez Solaz", edad: 27, sexo: "M", alta:new Date()})

O bien utilizando el Batch insert:

    db.usuario.insert([{_id:NumberLong(5), nombre:"Juan", apellidos:"García Castellano", edad: 23, sexo:"H", alta:new Date()}, {_id:NumberLong(6), nombre:"Beatriz", apellidos:"Perez Solaz", edad: 27, sexo: "M", alta:new Date()}])


2-. Escribe el comando para recuperar todos los documentos de la colección "usuario".

    db.usuario.find().pretty()

3-. Los usuarios pueden pertenecer a tantos grupos como deseen, y guardamos dichos grupos en un
campo de tipo Array dentro de cada usuario. Inserta el nuevo usuario Jorge Lopez Sevilla, con
identificador 7, que no nos dice su edad, y que pertenece a los grupos "baloncesto", "cocina" y
"novela historica".

    db.usuario.insert({_id:NumberLong(7), nombre:"Jorge", apellidos:"Lopez Sevilla", sexo:"H", grupos: ["baloncesto","cocina","novela histórica"]})

4-. Juan García, con identificador 5 se da de baja. Escribe la sentencia para borrarlo.

    db.usuario.remove({_id:NumberLong(5)})

5-. El usuario Beatriz, con identificador 6, se apunta a tres grupos, que son "novela historica" y
"baile". Escribe la sentencia para actualizar estos campos en su documento sin volver a informar
el resto. Recuerde que los grupos se guardan como array.

    db.usuario.update({_id:6}, {$set: {grupos: ["novela histórica","baile"]}})

comprobar que ha cambiado

    db.usuario.find({_id:6}).pretty()

6-. En nuestra red social también pueden registrarse empresas, que guardamos en la colección
"empresa", también con identificador Long, y para las que por el momento solo almacenamos el
nombre de la empresa. Escribe el comando para insertar la empresa "Jardinería Gardenia" con
identificador 10.

    db.empresa.insert({_id:NumberLong(10), nombre:"Jardinería Gardenia"})

ver las collecciones que tenemos

    show collections

7-. Ahora debes actualizar los datos de la empresa "Jardinería Gardenia" informando los siguientes
campos:
- Razón social. Debes especificarlo como un documento embebido con dirección "Calle las
Palmeras", número 8 de la población Torrente.
- Sector, servicios.
- Web en http://www.jardineriagardenia.com

```
db.empresa.update({nombre: "Jardinería Gardenia"}, {$set: {razonSocial:{direccion:"Calle las Palmeras", numero:8, población:"Torrente"}, sector: "servicios", web:"http://www.jardineriagardenia.com"}})
```
8-. Vamos a llevar un contador de los seguidores de las empresas de la red social, y lo haremos
sobre el campo "seguidores" de la colección "empresa". Cinco usuarios han marcado seguir a la
empresa "Jardinería Gardenia". Escribe el comando para incrementar en cinco los "seguidores".
Después se apuntan dos más. Escribe también el comando para incrementarlo.

Por último se borra uno. Escribe también el comando para decrementarlo.

    db.empresa.update({nombre: "Jardinería Gardenia"}, {$inc: {seguidores:5}})
    db.empresa.update({nombre: "Jardinería Gardenia"}, {$inc: {seguidores:2}})
    db.empresa.update({nombre: "Jardinería Gardenia"}, {$inc: {seguidores:-1}})

9-. Actualiza dentro de la razon social, el código postal de la empresa "Jardinería Gardenia" a 46009

    db.empresa.update({nombre: "Jardinería Gardenia"}, {$set: {razonSocial.codpostal:46009}})

10-. Elimina el campo "sector" de la empresa "Jardinería Gardenia", dejando intactos el resto de
campos.

    db.empresa.update({nombre: "Jardinería Gardenia"}, {$unset: {sector:1}})

11-. El usuario Beatriz, con identificador 6 se apunta al grupo "teatro". Escribe el comando para añadir dicho grupo a su array.

    db.usuario.update({_id:NumberLong(6)}, {$push:{grupos:"teatro"}})

12-. El usuario con identificador 6 se borra del grupo "baile". Escribe la sentencia para eliminarlo de su array de grupos.

    db.usuario.update({_id:NumberLong(6)}, {$pull:{grupos:"baile"}})

13-. Como en cualquier red social, los usuarios pueden introducir comentarios. En nuestro caso los comentarios tienen varios campos, que son.
- Titulo
- Texto
- Grupo al que pertenece el comentario.
- Fecha
Guardaremos los comentarios en la propia colección de "usuario", en un nuevo campo
"comentarios", que será un Array de objetos con las propiedades anteriores.
Además, a nivel de usuario, también vamos a llevar un contador del número de comentarios
totales que realiza cada usuario, en un campo "total_comentarios", que iremos incrementando
cada vez que insertemos un nuevo comentario.
Escribe el comando para insertar un nuevo comentario para el usuario Jorge Lopez Sevilla en el
grupo "novela historica", a la vez que incrementa en uno el "total_comentarios".

```
    db.usuario.update({_id:NumberLong(7)}, {$push:{comentarios:{titulo:"Alatriste", texto: "Me encanta la Novela", grupo:"Novela Historica", fecha:new Date()}},$inc:{total_comentarios:1}})
```

## consultas (ejemplos están en la aplicación)

1.- Introducir el comando para recuperar todos los hombres (sexo H), mostrando su Nombre,
Apellidos y la fecha en que se registraron.

    db.usuario.find({sexo:"V"}, {nombre:1, apellidos:1, fecha:1})

2.- Introducir el comando para recuperar todas las mujeres (sexo M), que estén activas y ubicadas
en España (Espana), eliminando de los resultados devueltos el campo comentarios y mensajes.

    db.usuario.find({$and:[{sexo:"M"}, {activo:true}, {"ubicacion.pais":"Espana"}]}, {comentarios:0, mensajes:0})

3.- Introducir el comando para recuperar aquellos usuarios que tengan más de un comentario.

    db.usuario.find({total_comentarios:{$gt:1}})

4.- Introducir el comando para recuperar los usuarios nacidos antes del 1 de Enero de 2000.

    db.usuario.find({nacimiento:{$lt:ISODate("2000-01-01T00:00:00Z")}})

5.- Introducir el comando para recuperar los usuarios nacidos entre el 1 de Enero de 1979 y el 31
de Diciembre de 2000, ordenados por fecha de nacimiento ascendentemente.

    db.usuario.find({$and:[{nacimiento:{$gte:ISODate("1979-01-01T00:00:00Z")}}, {nacimiento:{$lte:ISODate("2000-12-01T00:00:00Z")}}]}).sort({nacimiento:1})

6.- Introducir el comando para recuperar los usuarios en que alguno de sus apellidos sea García.

    db.usuario.find({apellidos:/Garcia/i})

7.- Introducir el comando para recuperar los usuarios que aún no tienen ningún amigo.

    db.usuario.find({$or:[{amigos:{$size:0}},{amigos:{$exists:0}}]}).pretty()

8.- Uno de los usuarios que todavía no tiene amigos es el usuario con identificador NumberLong(3),
Laura Gorriz Pardo, de Castellon, España. Para proponerle contactos, recuperamos los usuarios de la
ubicación Castellon, España (Espana) y que estén activos. Introduzca el comando de búsqueda.

    db.usuario.find({$and:[{"ubicacion.ciudad":"Castellon"}, {"ubicacion.pais":"Espana"},{activo:true}]})

9.- Introducir el comando para recuperar los usuarios que estén apuntados al grupo "cocina" o al
grupo "teatro"

    db.usuario.find({grupos:{$in:["cocina", "teatro"]}})


10.- Introducir el comando para recuperar los usuarios que estén apuntados a los grupos "cine" y "teatro".

    db.usuario.find({grupos:{$all:["cine", "teatro"]}})

11.a- Recuperar los usuarios (id, nombre y apellidos) que hayan hecho un comentario en el grupo    "futbol" y que hayan hablado de "brasil" o de "argentina".

    db.usuario.find({$and:[{"comentarios.grupo":"futbol"},    {$or:[{"comentarios.texto":/Brasil/i},{"comentarios.texto":/Argentina/i}]}]},{_id:1,nombre:1,apellidos:1})

11.b- Recuperar los usuarios (id, nombre y apellidos) que hayan hecho un comentario en el grupo    "futbol" y que hayan hablado de "brasil" o de "argentina".

    db.usuario.find({$and:[{"comentarios.grupo":"futbol"},
    {"comentarios.texto":{$in:[/Brasil/i,/Argentina/i]}}]},{_id:1, nombre:1, apellidos:1})

12.- Recuperar el nombre, apellidos y fecha de los tres usuarios más antiguos, es decir, aquellos con fecha más antigua.

    db.usuario.find({ },{nombre:1, apellidos:1, fecha:1}).limit(3).sort({fecha:1})

13.- Introducir el comando para recuperar los comentarios hechos por personas que han comentado    algo en el grupo “futbol” ordenados por fecha del comentario descendentemente.

    db.usuario.find({"comentarios.grupo":"futbol"}, {nombre:1,"comentarios.texto":1})
