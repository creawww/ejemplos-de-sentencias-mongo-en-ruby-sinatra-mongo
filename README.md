# Ejemplos de sentencias Mongo en ruby-sinatra-mongo

Entorno docker de Ruby Mongo, donde se cargar una base de datos de ejemplo y se corren una serie de sentencias mongo sobre esta.

En el archivos Ejercicios.md hay una visita guiada por mongo que podemos ejecutar directamente sobre el contenedor de mongo e ir viendo como se ejecutan.

[Ejercicios](Ejercicios.md)


## Puesta en marcha ejemplos con ruby

Para que corran las pruebas tenemos primero que importar la base de datos

tenemos que arrancar los contenedores con

    docker-compose up


#### IMPORTAR BD para correr los ejemplos

para correr los ejemplos tenemos que importar la BD

1. copiaremos el archivo dentro de contenedor
```
    docker cp redsocial.json s_mongo:/root/
```
2. entramos en el contenedor    
```
    docker exec -it s_mongo /bin/bash
```
entrar a la carpeta /root e importar el archivo

    cd root

    mongoimport --jsonArray --db redSocial --collection usuario --file redsocial.json

Con esto ya tendermos nuestra base de datos redsocial importada y podemos correr el programa de sinatra para ver las sentencia


    http://localhost:3000/ #ejemplo en json de la tabla usuarios

    http://localhost:3000/list/ # list de sentencia
