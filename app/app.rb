require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'mongo'
require 'json'

client = Mongo::Client.new([ 'db:27017' ], :database => 'redSocial')

get '/' do
   client[:usuario].find().to_a.to_json
end

get '/list/' do
  erb :list, locals: { data: querys}
end

get '/example/:id' do
  query = querys[params[:id].to_i]
  data= eval query[:query]
  erb :table, locals: {question: query[:question],title: query[:query], data: data  }
end

def ISODate(date)
  DateTime.strptime(date, '%Y-%m-%dT%H:%M:%S%z').to_time.utc
end

def querys
  [
    {question: "0.- Coleccion Usuarios", query: 'client[:usuario].find()' },
    {question: "1.- Recuperar todos los hombres (sexo H), mostrando su Nombre, Apellidos y la fecha en que se registraron.", query: 'client[:usuario].find({sexo:"V"}, projection:{nombre: 1, apellidos: 1, fecha: 1})' },
    {question: "2.- Recuperar todas las mujeres (sexo M), que estén activas y ubicadas en España (Espana), eliminando de los resultados devueltos el campo comentarios y mensajes.", query: 'client[:usuario].find({"$and":[{sexo: "M"}, {activo: true}, {"ubicacion.pais": "Espana"}]}, projection: {comentarios:0, mensajes:0} )' },
    {question: "3.- Recuperar aquellos usuarios que tengan más de un comentario.", query: 'client[:usuario].find({total_comentarios:{"$gt":1}}, projection:{nombre: 1, apellidos: 1, total_comentarios: 1,comentarios:1})' },
    {question: "4.- Recuperar los usuarios nacidos antes del 1 de Enero de 2000.", query: 'client[:usuario].find({nacimiento:{"$lt":ISODate("2000-01-01T00:00:00Z")}},projection:{nombre: 1, apellidos: 1, nacimiento: 1})' },
    {question: "5.- Recuperar los usuarios nacidos entre el 1 de Enero de 1979 y el 31 de Diciembre de 2000, ordenados por fecha de nacimiento ascendentemente.", query: 'client[:usuario].find({"$and":[{nacimiento:{"$gte": ISODate("1979-01-01T00:00:00Z")}}, {nacimiento:{"$lte":ISODate("2000-12-01T00:00:00Z")}}]}, projection:{nombre: 1, apellidos: 1, nacimiento: 1}).sort({nacimiento:1})' },
    {question: "6.- Recuperar los usuarios en que alguno de sus apellidos sea García.", query: 'client[:usuario].find({apellidos:/Garcia/i}, projection:{nombre: 1, apellidos: 1})' },
    {question: "7.- Recuperar los usuarios que aún no tienen ningún amigo.", query: 'client[:usuario].find({"$or":[{amigos:{"$size":0}},{amigos:{"$exists":0}}]})' },
    {question: "8.- Recuperamos los usuarios de la ubicación Castellon, España (Espana) y que estén activos.", query: 'client[:usuario].find({"$and":[{"ubicacion.ciudad":"Castellon"}, {"ubicacion.pais":"Espana"},{activo:true}]})'},
    {question: "9.- Los usuarios que estén apuntados al grupo 'cocina' o al
    grupo 'teatro'", query: 'client[:usuario].find({grupos:{"$in":["cocina", "teatro"]}})' },
    {question: "10 .- Recuperar los usuarios que estén apuntados a los grupos 'cine' y 'teatro'.", query: 'client[:usuario].find({grupos:{"$all":["cine", "teatro"]}}, projection:{nombre: 1, apellidos: 1,activo:1,	fecha:1, grupos: 1})'  },
    {question: "11.a- Recuperar los usuarios (id, nombre y apellidos) que hayan hecho un comentario en el grupo  'futbol' y que hayan hablado de 'brasil' o de 'argentina'.", query: 'client[:usuario].find({"$and":[{"comentarios.grupo":"futbol"},    {"$or":[{"comentarios.texto":/Brasil/i},{"comentarios.texto":/Argentina/i}]}]},projection: {_id:1,nombre:1,apellidos:1,comentarios:1})' },
    {question: "11.b- Recuperar los usuarios (id, nombre y apellidos) que hayan hecho un comentario en el grupo  'futbol' y que hayan hablado de 'brasil' o de 'argentina.", query: 'client[:usuario].find({"$and":[{"comentarios.grupo":"futbol"},
    {"comentarios.texto":{"$in":[/Brasil/i,/Argentina/i]}}]},projection:{_id:1, nombre:1, apellidos:1,comentarios:1})' },
    {question: "12.- Recuperar el nombre, apellidos y fecha de los tres usuarios más antiguos, es decir, aquellos con fecha más antigua.", query: 'client[:usuario].find({ },projection:{nombre:1, apellidos:1, fecha:1}).limit(3).sort({fecha:1})' },
    {question: "13.- Recuperar los comentarios hechos por personas que han comentado algo en el grupo “futbol” ordenados por fecha del comentario descendentemente.", query: 'client[:usuario].find({"comentarios.grupo":"futbol"},projection:{nombre:1,"comentarios.texto":1})' },
    {question: "14.- Aplanar (convertir un array en varios registros) los comentarios sobre futbol .", query: 'client[:usuario].aggregate([{"$project": {"comentarios": 1}}, {"$unwind": "$comentarios" }, {"$match": {"comentarios.grupo": "futbol"}}])' },    
  ]
end
